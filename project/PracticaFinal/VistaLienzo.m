//
//  VistaPrincipal.m
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//
#import "VistaLienzo.h"

@implementation VistaLienzo

static NSRect functRect={ -400, -400, 2*400, 2*400 };

-(id)initWithFrame:(NSRect)frame{
    self=[super initWithFrame:frame];
    if(!self){
        return nil;
    }
    primerPunto = NSMakePoint(0,0);
    puntoActual = NSMakePoint(0,0);
    clickActivo = FALSE;
    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    
    [[NSColor darkGrayColor ] setFill];
    [super drawRect:dirtyRect];
    
    
    
    
    NSRect bounds=[self bounds];
    NSGraphicsContext *ctx = [NSGraphicsContext currentContext];
    [NSBezierPath fillRect:bounds];
    
    
    [[NSColor blackColor] setStroke];
    [[NSColor redColor] setFill];
    
    /*----SISTEMA DE COORDENADAS----------------*/
    
    [[NSColor whiteColor] set];
    
    for (int i = 0; i < 60; i++)
    {
     //   NSLog(@"fat lip");
        NSPoint pinicio = NSMakePoint(0, 50 * i);
        NSPoint pfin  = NSMakePoint(10000, 50 * i) ;
        [NSBezierPath strokeLineFromPoint:pinicio
                                  toPoint:pfin];
        
    }
    
    for (int i = 0 ; i < 60 ; i ++)
    {
    //    NSLog(@"anarchy in UK");
        NSPoint pinicio = NSMakePoint(50 * i, 0);
        NSPoint pfin  = NSMakePoint(50 * i, 10000) ;
        [NSBezierPath strokeLineFromPoint:pinicio
                                  toPoint:pfin];
    }
    
    
    
    
    /*------COORDENADAS NUMÉRICAS---------------*/
    
    for( int i = 1; i < 60 ; i++)
    {
        int k;
        NSPoint puntoDeCoordenadaV = NSMakePoint(5 , 50 * i);
        k = 50 * i;
        NSString *stringTag=[NSString stringWithFormat:@"%d", k];
        tag = [[NSTextField alloc] initWithFrame:NSMakeRect(puntoDeCoordenadaV.x,puntoDeCoordenadaV.y, 50, 20)];
        
        [tag setStringValue:stringTag];
        [tag setBezeled:NO];
        [tag setDrawsBackground:NO];
        [tag setEditable:NO];
        [tag setSelectable:NO];
        [self addSubview:tag];
    }
    
    
    for( int i = 1; i < 60 ; i++)
    {
        int k;
        NSPoint puntoDeCoordenadaH = NSMakePoint(50 * i , 5);
        k = 50 * i;
        NSString *stringTag=[NSString stringWithFormat:@"%d", k];
        tag = [[NSTextField alloc] initWithFrame:NSMakeRect(puntoDeCoordenadaH.x,puntoDeCoordenadaH.y, 50, 20)];
        
        [tag setStringValue:stringTag];
        [tag setBezeled:NO];
        [tag setDrawsBackground:NO];
        [tag setEditable:NO];
        [tag setSelectable:NO];
        [self addSubview:tag];
    }
    
    
    [controlador dibujarFiguras:bounds
            withGraphicsContext:ctx];
    
    NSString *tipo;
    
    if(botonLinea.state==TRUE) tipo=@"LINEA";
    if(botonRect.state==TRUE) tipo=@"RECTANGULO";
    if(botonElipse.state==TRUE)  tipo=@"ELIPSE";
    
    if ([tipo isEqualToString:@"LINEA"])
    {
    
    [[NSColor greenColor] set];
    [NSBezierPath strokeLineFromPoint:primerPunto
                              toPoint:puntoActual];
    }
    
    if ([tipo isEqualToString:@"RECTANGULO"])
    {
        
        float anchoRectangulo = 0.0;
        float altoRectangulo =  0.0;
        float origenX = 0.0;
        float origenY = 0.0;
        
        
        anchoRectangulo = puntoActual.x - primerPunto.x;
        altoRectangulo  = puntoActual.y - primerPunto.y;
        
        // Tratamiento de las dimensiones
        
        if (anchoRectangulo < 0){
            anchoRectangulo = anchoRectangulo * (-1);
        }
        
        if (altoRectangulo < 0){
            altoRectangulo = altoRectangulo * (-1);
        }
        
        
        NSLog(@"1a Coord: %f %f", primerPunto.x, primerPunto.y);
        NSLog(@"2a Coord: %f %f", puntoActual.x,puntoActual.y);
        NSLog(@"ancho %f alto %f", anchoRectangulo, altoRectangulo);
        
        //Primer cuadrante
        
        if ((puntoActual.x > primerPunto.x) && (puntoActual.y > primerPunto.y)){
            
            origenX = primerPunto.x;
            origenY = primerPunto.y;
           
        }
        
        //Segundo cuadrante
        
        if ((puntoActual.x < primerPunto.x) && (puntoActual.y > primerPunto.y)){
            
            origenX = puntoActual.x;
            origenY = primerPunto.y;
            
           
        }
        
        // Tercer cuadrante
        
        if ((puntoActual.x < primerPunto.x) && (puntoActual.y < primerPunto.y)){
            
            origenX = puntoActual.x;
            origenY = puntoActual.y;
       
            
            
        }
        
        
        // Cuarto cuadrante
        
        if ((puntoActual.x > primerPunto.x) && (puntoActual.y < primerPunto.y)){
            
            origenX = primerPunto.x;
            origenY = puntoActual.y;
            
         
        }
        
        
        unRectangulo = NSMakeRect(origenX, origenY, anchoRectangulo, altoRectangulo);
    
    
    
        NSRect b =  NSMakeRect(origenX,origenY,anchoRectangulo,altoRectangulo);
        [[NSColor greenColor] set];
        [NSBezierPath strokeRect:b];
    
    }

    if ([tipo isEqualToString:@"ELIPSE"])
    {
        
        float anchoRectangulo = 0.0;
        float altoRectangulo =  0.0;
        float origenX = 0.0;
        float origenY = 0.0;
        
        
        anchoRectangulo = puntoActual.x - primerPunto.x;
        altoRectangulo  = puntoActual.y - primerPunto.y;
        
        // Tratamiento de las dimensiones
        
        if (anchoRectangulo < 0){
            anchoRectangulo = anchoRectangulo * (-1);
        }
        
        if (altoRectangulo < 0){
            altoRectangulo = altoRectangulo * (-1);
        }
        
        
        NSLog(@"1a Coord: %f %f", primerPunto.x, primerPunto.y);
        NSLog(@"2a Coord: %f %f", puntoActual.x,puntoActual.y);
        NSLog(@"ancho %f alto %f", anchoRectangulo, altoRectangulo);
        
        //Primer cuadrante
        
        if ((puntoActual.x > primerPunto.x) && (puntoActual.y > primerPunto.y)){
            
            origenX = primerPunto.x;
            origenY = primerPunto.y;
            
        }
        
        //Segundo cuadrante
        
        if ((puntoActual.x < primerPunto.x) && (puntoActual.y > primerPunto.y)){
            
            origenX = puntoActual.x;
            origenY = primerPunto.y;
            
            
        }
        
        // Tercer cuadrante
        
        if ((puntoActual.x < primerPunto.x) && (puntoActual.y < primerPunto.y)){
            
            origenX = puntoActual.x;
            origenY = puntoActual.y;
            
            
            
        }
        
        
        // Cuarto cuadrante
        
        if ((puntoActual.x > primerPunto.x) && (puntoActual.y < primerPunto.y)){
            
            origenX = primerPunto.x;
            origenY = puntoActual.y;
            
            
        }
        
        
        unRectangulo = NSMakeRect(origenX, origenY, anchoRectangulo, altoRectangulo);
        
        [[NSColor greenColor] set];
      NSBezierPath *circo = [NSBezierPath bezierPath];
        [circo appendBezierPathWithOvalInRect:unRectangulo];
        [circo stroke];
        
        
       
        
        //[NSBezierPath stro:b];
        
    }


    NSLog(@"SetNeedsDisplay");
    
}

- (void)mouseDown:(NSEvent *) evento
{
    NSString *tipo;
    
    if(botonLinea.state==TRUE) tipo=@"LINEA";
    if(botonRect.state==TRUE) tipo=@"RECTANGULO";
    if(botonElipse.state==TRUE)  tipo=@"ELIPSE";
    
    
        NSPoint punto = [self convertPoint:[evento locationInWindow] fromView:nil];
        
        //Guarda las coordenadas hasta el siguiente clic
        primerPunto = punto;
        //Crear area de seguimiento
        NSLog(@"areaSeguimiento");
        areaSeguimiento = [self addTrackingRect:[self visibleRect]
                                          owner:self
                                       userData:nil
                                   assumeInside:YES];
        
        [[self window] setAcceptsMouseMovedEvents:YES];
        
        
        
    
}
- (void)mouseEntered:(NSEvent *)evento{
    [[self window] setAcceptsMouseMovedEvents:YES];
}

- (void)mouseExited:(NSEvent *)evento{
    [[self window] setAcceptsMouseMovedEvents:NO];
}
- (void)mouseMoved:(NSEvent *)evento{
    
    NSLog(@"Seeeeeeeeguimiento");
    puntoActual = [self convertPoint:[evento locationInWindow] fromView:nil];
    [self setNeedsDisplay:YES];
}

-(void) mouseDragged:(NSEvent *)theEvent
{
    clickActivo = TRUE;
    puntoActual = [self convertPoint:[theEvent locationInWindow] fromView:nil];
    [self setNeedsDisplay:YES];
}

-(void) mouseUp:(NSEvent *)theEvent
{
    NSMutableDictionary  * dic = [[NSMutableDictionary alloc] init];

    
    NSString *tipo;
    float anchoRectangulo;
    float altoRectangulo;
    
    if(botonLinea.state==TRUE) tipo=@"LINEA";
    if(botonRect.state==TRUE) tipo=@"RECTANGULO";
    if(botonElipse.state==TRUE)  tipo=@"ELIPSE";
    
    FiguraAbstracta *figuraNueva;
    
    NSColor *colorseleccionado = [NSColor yellowColor];
    NSColor *colorthick = [NSColor blackColor];
    double x1=primerPunto.x;
    double y1=primerPunto.y;
    double param1=puntoActual.x;
    double param2=puntoActual.y;
    
    if (clickActivo)
    {
        clickActivo = FALSE;
        
        if (![tipo isEqualToString:@"LINEA"])
        {
            x1 = unRectangulo.origin.x;
            y1 = unRectangulo.origin.y;
            param1 = unRectangulo.size.height;
            param2 = unRectangulo.size.width;
            
            
        }
        
        
        figuraNueva =[FiguraAbstracta crearFiguraWithTipo:tipo
                                                withColor:colorseleccionado
                                            withColorBord:colorthick
                                                   withx1:x1
                                                   withy1:y1
                                               withparam1:param1
                                               withparam2:param2];
        
        [dic setValue:figuraNueva forKey:@"Informacion"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ETPanelChangeNotification"
                                                            object:dic];
        
        primerPunto = NSMakePoint(0,0);
        puntoActual = NSMakePoint(0,0);
        unRectangulo = NSMakeRect(0, 0, 0, 0);
        unCirc = NSMakeRect(0, 0, 0, 0);
        
    }
}

-(IBAction)seleccionFiguraChange:(id)sender{
    NSLog(@"Radio principal ha cambiado");
    
    
}


@end
