//
//  ControladorPrincipal.h
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class VistaLienzo;

@class controladorVentanaAux;

@interface Controladora : NSView<NSWindowDelegate>{

    IBOutlet VistaLienzo *miVista;
    
   
    NSMutableArray * _arrayFiguras;
    
    controladorVentanaAux * panelController;
    
}

-(IBAction)abrirpanel:(id)sender;


-(void)dibujarFiguras:(NSRect)b
  withGraphicsContext:(NSGraphicsContext *)ctx;

@end
