//
//  controladorVentanaAux.h
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FiguraAbstracta.h"

@interface controladorVentanaAux : NSWindowController< NSTableViewDataSource, NSTableViewDelegate, NSControlTextEditingDelegate>{
    
    
    
    
    IBOutlet NSButton *botonLinea;
    IBOutlet NSButton *botonRect;
    IBOutlet NSButton *botonElipse;
    IBOutlet NSColorWell *colorwell;
    IBOutlet NSColorWell *colorBorde;
    IBOutlet NSButton *insertarFiguraButton;
    IBOutlet NSButton *eliminarFiguraButton;
    IBOutlet NSButton *modificarFiguraButton;
    IBOutlet NSButton *cerrarPanelButton;
    IBOutlet NSTextField * TextFieldX1;
    IBOutlet NSTextField * TextFieldY1;
    IBOutlet NSTextField * TextFieldParam1;
    IBOutlet NSTextField * TextFieldParam2;
    
    IBOutlet NSTextField * labelParam1;
    IBOutlet NSTextField * labelParam2;
    
    IBOutlet NSTableView * aTableView;
    
    // Fila seleccionada de la tabla
    NSInteger aRowSelected;
    
    //Modelo: Matriz de Figuras
    NSMutableArray * MatrizFiguras;
}

@property FiguraAbstracta * figurasActual;

- (IBAction)insertarFigura:(id)sender;
- (IBAction)eliminarFigura:(id)sender;

- (IBAction)actualizarFigura:(id)sender;


-(IBAction)seleccionFiguraChange:(id)sender;




- (id) initWithArrayDatos: (NSMutableArray *) array;

@end
