//
//  ControladorPrincipal.m
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//

#import "Controladora.h"
#import "FiguraAbstracta.h"
#import "controladorVentanaAux.h"
#import "VistaLienzo.h"

@implementation Controladora
-(id) init{
    self=[super init];
    if(self){
        NSLog(@"En init\n");
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self
               selector:@selector(handlePanelChange:)
                   name:@"ETPanelChangeNotification"
                 object:nil];
    }
    _arrayFiguras = [[NSMutableArray alloc]init];

    return self;
}

-(void) handlePanelChange: (NSNotification*) notificacion
{
    NSLog(@"Notificacion recibida");
    
    NSMutableDictionary * dic = [notificacion object];
    FiguraAbstracta *figura = [[FiguraAbstracta alloc]init];
 
    NSUInteger i = 0;
    if (dic[@"ACTUALIZACION"] != nil)
    {
        
        NSLog(@"ACTUALIZACION");
        
      

        [miVista setNeedsDisplay:YES];
    }
    else if(dic[@"Informacion"]!= nil){  /*---AGREGAR FIGURA-----*/
        figura = dic[@"Informacion"];
        
    
            //Codigo para modificar
            NSLog(@"Agrego Figuras LIENZO");
            FiguraAbstracta *figuraNueva;
        
       
        
        
       
        
        
        figuraNueva =[FiguraAbstracta crearFiguraWithTipo:figura.tipoFigura
                                                withColor:figura.colorRellenoFigura
                                            withColorBord:figura.colorBorde
                                                   withx1:figura.x1Figura
                                                   withy1:figura.y1Figura
                                               withparam1:figura.param1Figura
                                               withparam2:figura.param2Figura];
        
        
        
        [_arrayFiguras addObject:figuraNueva];
        
        [miVista setNeedsDisplay:YES];

    

    }
    
    else if(dic[@"Eliminar"]!= nil){
    
        NSLog(@"notificacion  eliminar recibida");
        
        NSString *s =[NSString stringWithFormat:@"%@ ", [dic objectForKey:@"Eliminar"]];
        
        NSLog(@" valor de s %@",s);
        NSUInteger b=[s intValue];
        
        
        [_arrayFiguras removeObjectAtIndex:b];
        
          [miVista setNeedsDisplay:YES];
    }
    
    else{
        
        figura = dic[@"Modificacion"];
    
        NSString *s =[NSString stringWithFormat:@"%@ ", [dic objectForKey:@"Columna"]];
        
        NSLog(@" valor de s %@",s);
        NSUInteger b=[s intValue];
        
        
        
        FiguraAbstracta *figuraNueva;
        
        
        figuraNueva =[FiguraAbstracta crearFiguraWithTipo:figura.tipoFigura
                                        withColor:figura.colorRellenoFigura
                                            withColorBord:figura.colorBorde
                                           withx1:figura.x1Figura
                                           withy1:figura.y1Figura
                                       withparam1:figura.param1Figura
                                       withparam2:figura.param2Figura];
        
        
        [_arrayFiguras removeObjectAtIndex:b];
        
        [miVista setNeedsDisplay:YES];
        
        [_arrayFiguras addObject:figuraNueva];
        
        [miVista setNeedsDisplay:YES];
        
        
        
        
    
    }

}
-(void)dibujarFiguras:(NSRect)b
  withGraphicsContext:(NSGraphicsContext *)ctx
{
    for (FiguraAbstracta *f in _arrayFiguras){
        [f dibujarFigura:b
              withFigura:f
     withGraphicsContext:ctx];
        NSLog(@"DIBUJANDO UNA FIGURITA");
    }
    
    
    
    
    
    
    
}

-(void) dealloc
{
    _arrayFiguras=nil;
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
}



- (BOOL)windowShouldClose:(id)sender
{
    NSAlert *alertWindow = [[NSAlert alloc] init];
    
    [alertWindow setMessageText:@"¿Está seguro de que desea cerrar la ventana?"];
    [alertWindow addButtonWithTitle:@"No"];
    [alertWindow addButtonWithTitle:@"Si"];
    [alertWindow setInformativeText:@"La aplicación se cerrará si se pulsa si"];
    
    
    if ([alertWindow runModal] == NSAlertFirstButtonReturn)
        return NO;
    
    else
    {
        [NSApp terminate:self];
        return YES;
    }
}


-(IBAction)abrirpanel:(id)sender{
    
    if(!panelController)
    {
        panelController = [[controladorVentanaAux alloc] initWithArrayDatos: _arrayFiguras];
      //  panelController = [[controladorVentanaAux alloc] init]
    }
    
    NSLog(@"Mostrar panel ajustes \n");
    [panelController showWindow:self];
}

-(IBAction)windowDidResize:(NSNotification *)notification{
}

@end
