//
//  controladorVentanaAux.h
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//

#import "controladorVentanaAux.h"

@interface controladorVentanaAux ()

@end

@implementation controladorVentanaAux
@synthesize figurasActual;

- (void)windowDidLoad
{
    [super windowDidLoad];
    [eliminarFiguraButton setEnabled: NO];
    [modificarFiguraButton setEnabled: NO];
   
    NSLog(@"Fichero NIB cargado");
}

- (id) init
{
    if(![super initWithWindowNibName:@"controladorVentanaAux"])
        return nil;
    
    return self;
}

- (id) initWithArrayDatos: (NSMutableArray *) array
{
    if ([self init]) {
        MatrizFiguras = array;
    }
    return self;
}

- (id) initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if(self){
        //Initialization code here
        aRowSelected = -1;
    }
    return self;
}


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [MatrizFiguras count];
}


- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    
    FiguraAbstracta *f = [MatrizFiguras objectAtIndex:row];
    NSString *identifier = [tableColumn identifier];
    return [f valueForKey:identifier];
}


-(IBAction)insertarFigura:(id)sender{
    NSMutableDictionary  * dic = [[NSMutableDictionary alloc] init];
    
    FiguraAbstracta * clavefigura;
    
    NSRect b;
    double x1=TextFieldX1.doubleValue;
    double y1=TextFieldY1.doubleValue;
    double param1=TextFieldParam1.doubleValue;
    double param2=TextFieldParam2.doubleValue;
    NSString *tipo;
    
    NSColor *colorseleccionado = [colorwell color];
    NSColor *colorthick = [colorBorde color];
    
    if(botonLinea.state==TRUE) tipo=@"LINEA";
    if(botonRect.state==TRUE) tipo=@"RECTANGULO";
    if(botonElipse.state==TRUE)  tipo=@"ELIPSE";

    
    
    FiguraAbstracta *figuraNueva;
    figuraNueva =[FiguraAbstracta crearFiguraWithTipo:tipo
                                            withColor:colorseleccionado
                                        withColorBord:colorthick
                                               withx1:x1
                                               withy1:y1
                                           withparam1:param1
                                           withparam2:param2];
    
    
                   
    
    [MatrizFiguras addObject:figuraNueva];
    [dic setValue:figuraNueva forKey:@"ACTUALIZACION"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ETPanelChangeNotification"
                                                        object:dic];
    
   

    [aTableView reloadData];
}



-(IBAction)eliminarFigura:(id)sender{
    NSMutableDictionary  * dic = [[NSMutableDictionary alloc] init];
      NSUInteger row = [aTableView selectedRow];
      NSString *s=[@(row) stringValue];// pasar de entero a string
    [MatrizFiguras removeObjectAtIndex:row];
    
    [dic setValue:s forKey:@"ACTUALIZACION"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ETPanelChangeNotification"
                                                        object:dic];
    
    [aTableView reloadData];
    
    
    


}



-(IBAction)actualizarFigura:(id)sender{
    //NSMutableDictionary  * dic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary  * dic = [[NSMutableDictionary alloc] init];
    FiguraAbstracta * clavefigura;
    NSRect b;
    NSUInteger row = [aTableView selectedRow];
    NSString *s=[@(row) stringValue];// pasar de entero a string
    
    double x1=TextFieldX1.doubleValue;
    double y1=TextFieldY1.doubleValue;
    double param1=TextFieldParam1.doubleValue;
    double param2=TextFieldParam2.doubleValue;
    NSString *tipo;
    
    NSColor *colorseleccionado = [colorwell color];
    NSColor *colorthick = [colorBorde color];
    
    if(botonLinea.state==TRUE) tipo=@"LINEA";
    if(botonRect.state==TRUE) tipo=@"RECTANGULO";
    if(botonElipse.state==TRUE)  tipo=@"ELIPSE";
    
    FiguraAbstracta *figuraNueva;
    figuraNueva =[FiguraAbstracta crearFiguraWithTipo:tipo
                                            withColor:colorseleccionado
                                        withColorBord:colorthick
                                               withx1:x1
                                               withy1:y1
                                           withparam1:param1
                                           withparam2:param2];
    
    [dic setValue:figuraNueva forKey:@"ACTUALIZACION"];
   

    
    [MatrizFiguras removeObjectAtIndex:row];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ETPanelChangeNotification"
                                                        object:dic];
    
    [MatrizFiguras addObject:figuraNueva];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ETPanelChangeNotification"
                                                        object:dic];
    

    
    [aTableView reloadData];
    
    [insertarFiguraButton setEnabled:YES];
    

}


-(void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSInteger row = [aTableView selectedRow];
        
    
    if (row == -1){
        [eliminarFiguraButton setEnabled:NO];
        [modificarFiguraButton setEnabled:NO];

        
            }
    else {
        FiguraAbstracta *f = [MatrizFiguras objectAtIndex:row];
        NSString *tipo=f.tipoFigura;
        
        
        TextFieldX1.doubleValue=f.x1Figura;
        TextFieldY1.doubleValue=f.y1Figura;
        TextFieldParam1.doubleValue=f.param1Figura;
        TextFieldParam2.doubleValue=f.param2Figura;
        colorwell.color=f.colorRellenoFigura;
        colorBorde.color = f.colorBorde;
        
        
        if([tipo isEqual:@"LINEA"]) botonLinea.state=TRUE;
        if([tipo isEqual:@"RECTANGULO"]) botonRect.state=TRUE;
        if([tipo isEqual:@"ELIPSE"]) botonElipse.state=TRUE;
        
        
        [eliminarFiguraButton setEnabled:YES];
        [modificarFiguraButton setEnabled:YES];

        
        }
    
}


-(IBAction)seleccionFiguraChange:(id)sender{
    if((botonRect.state==TRUE)||(botonElipse.state==TRUE)){
        labelParam1.stringValue=@"Altura";
        labelParam2.stringValue=@"Anchura";
    }
    if(botonLinea.state==TRUE){
        labelParam1.stringValue=@"XDestino";
        labelParam2.stringValue=@"YDestino";
    }
    

}


    
@end
