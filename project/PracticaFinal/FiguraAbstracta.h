//
//  Figuras.h
//  PracticaFinal
//
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//


#import <Cocoa/Cocoa.h>

@interface FiguraAbstracta : NSObject{
    
    
    NSString *tipoFigura;
    NSRect rectanguloFigura;
    NSColor *colorRellenoFigura;
    NSColor *colorBorde;
    NSBezierPath *bezierPath;
    
    
    double x1Figura,y1Figura,param1Figura,param2Figura;
    
}
@property (nonatomic)NSString *tipoFigura;
@property (nonatomic)NSRect rectanguloFigura;
@property (nonatomic,copy) NSColor *colorRellenoFigura;
@property (nonatomic)double x1Figura;
@property (nonatomic, copy) NSColor *colorBorde;
@property (nonatomic)double y1Figura;
@property (nonatomic)double param1Figura;
@property (nonatomic)double param2Figura;



+(id) crearFiguraWithTipo:(NSString *) tipo
                withColor: (NSColor *)color
            withColorBord: (NSColor *) colorb
                   withx1: (double)x1
                   withy1: (double)y1
               withparam1: (double)param1
               withparam2: (double)param2;


-(id) initFiguraWithTipo:(NSString *) tipo
                withRect:(NSRect) rectangulo
               withColor: (NSColor *)color
          withColorBorde:(NSColor *)colorThickness
                  withx1: (double)x1
                  withy1: (double)y1
              withparam1: (double)param1
              withparam2: (double)param2;


+(id) verFigura;

-(void) dibujarFigura:(NSRect)b
           withFigura:(FiguraAbstracta *)figura
      withGraphicsContext:(NSGraphicsContext *)ctx;
@end
