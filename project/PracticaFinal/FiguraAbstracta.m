//
//  Figuras.m
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//

#import "FiguraAbstracta.h"

@implementation FiguraAbstracta

@synthesize tipoFigura,rectanguloFigura,x1Figura,y1Figura,param1Figura,param2Figura,colorRellenoFigura, colorBorde;
/*
+(id)verFigura{
    NSLog(tipoFigua)
}*/

+(id)crearFiguraWithTipo:(NSString *)tipo
          withColor:(NSColor *)color
           withColorBord:(NSColor *)colorb
             withx1:(double)x1
             withy1:(double)y1
         withparam1:(double)param1
         withparam2:(double)param2
{

        NSRect rectanguloc;
    
        rectanguloc.origin.x=x1;
        rectanguloc.origin.y=y1;
        rectanguloc.size.height=param1;
        rectanguloc.size.width=param2;
    
    FiguraAbstracta *unaFigura;
    
    unaFigura = [[FiguraAbstracta alloc]initFiguraWithTipo:tipo
                                          withRect:rectanguloc
                                         withColor:color
                                    withColorBorde:colorb
                                            withx1:x1
                                            withy1:y1
                                        withparam1:param1
                                        withparam2:param2];
                 
    
        
    
    return unaFigura;
}

-(id)initFiguraWithTipo:(NSString *)tipo
               withRect:(NSRect)rectangulo
              withColor:(NSColor *)color
         withColorBorde:(NSColor *)colorThickness
                 withx1:(double)x1
                 withy1:(double)y1
             withparam1:(double)param1
             withparam2:(double)param2

{
    self=[super init];
    if(!self){
        return nil;
    }
    bezierPath=[[NSBezierPath alloc]init];
    [self setTipoFigura:tipo];
    [self setRectanguloFigura:rectangulo];
    [self setColorRellenoFigura:color];
    [self setColorBorde:colorThickness];
    [self setX1Figura:x1];
    [self setY1Figura:y1];
    [self setParam1Figura:param1];
    [self setParam2Figura:param2];
    
    
     [bezierPath appendBezierPathWithRect:rectangulo];
    return self;
}

-(void)dibujarFigura:(NSRect)b
          withFigura:(FiguraAbstracta *)figura
 withGraphicsContext:(NSGraphicsContext *)ctx{
    NSAffineTransform *tf;
    
    [ctx saveGraphicsState];
    
    tf=[NSAffineTransform transform];
    
    [colorRellenoFigura set];
    
    
    [bezierPath removeAllPoints];
    //diferenciar los tipos segun el parametro tipoFigura
    
    if([figura.tipoFigura isEqual:@"LINEA"]){ //linea
        
        
        CGFloat grosor = 5.0;
        
        NSBezierPath *linea = [NSBezierPath bezierPath];
        NSPoint origen= NSMakePoint(x1Figura, y1Figura);
        NSPoint final= NSMakePoint(param1Figura, param2Figura);
        [linea setLineWidth:grosor];
        [linea moveToPoint:origen];
        [linea lineToPoint:final];
        [linea stroke];
        
        
    }
    if([figura.tipoFigura isEqual:@"RECTANGULO"]){//Rectangulo
        
        
        
        NSLog(@"Rectangulo");
        CGFloat grosor = 8.0;
        [bezierPath setLineWidth:grosor];
        [bezierPath appendBezierPathWithRect:rectanguloFigura];
        [bezierPath fill];
        [colorBorde set];
        [bezierPath stroke];
        
        
        

    }
    if([figura.tipoFigura isEqual:@"ELIPSE"]){//Elipse
        CGFloat grosor = 8.0;
        [bezierPath setLineWidth:grosor];
        //[bezierPath lineWidth:grosor];
        //setDefaultLineWidth(_ lineWidth: (float)8.0);
        [bezierPath appendBezierPathWithOvalInRect:rectanguloFigura];
        [bezierPath fill];
        [colorBorde set];
        [bezierPath stroke];
        
        
    }
        [ctx restoreGraphicsState];
    
}

@end
