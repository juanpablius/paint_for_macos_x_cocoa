//
//  VistaPrincipal.h
//  PracticaFinal
//
//  Created by Admin on 20/12/16.
//  Copyright © 2016 Juan Pablo Castro Hurtado. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Controladora.h"
#import "FiguraAbstracta.h"

@interface VistaLienzo : NSView{
    IBOutlet __weak Controladora *controlador;
      NSTextField *tag;
    
    IBOutlet NSButton *botonLinea;
    IBOutlet NSButton *botonRect;
    IBOutlet NSButton *botonElipse;

    
    
    NSPoint primerPunto;				// Coordenadas del primer punto entrado
    NSPoint puntoActual;                // Coordenadas del punto dónde esta el ratón
    NSTrackingRectTag areaSeguimiento;
    BOOL clickActivo;
    NSBezierPath *rectPath;
    NSRect unRectangulo;
    NSRect unCirc;
 
    
}
-(IBAction)seleccionFiguraChange:(id)sender;
- (void)mouseDragged:(NSEvent *)theEvent;
- (void)mouseDown:(NSEvent *) evento;
- (void)mouseMoved:(NSEvent *)evento;
- (void)mouseEntered:(NSEvent *)evento;
- (void)mouseExited:(NSEvent *)evento;
- (void) mouseUp:(NSEvent *)theEvent;
@end

